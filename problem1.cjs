let dataSet=require('./dataSet.cjs')
let ans=[];
let detailOfCarWithId= function detailOfCar(dataSet,Id)
{
    if(dataSet == null || dataSet == undefined){
        return ans;
    }
    if(Id == null || Id == undefined){
        return ans;
    }
    if(!Array.isArray(dataSet)){
        return ans;
    }
    if( typeof Id != 'number'){
        return ans;
    }
    for (let index = 0;index<dataSet.length;index++)
    {
        if(dataSet[index].id == Id)
        {
             ans[0] = dataSet[index]; 
             return ans;
        }
    }
    return ans;
}


//console.log(detailOfCarWithId);

module.exports=detailOfCarWithId;
